import logo from './logo.svg';
import './App.css';
import Ex_Shoes_Shop_Redux from './Ex_Shoes_Shop_Redux/Ex_Shoes_Shop_Redux';

function App() {
  return (
    <div className="App">
      <Ex_Shoes_Shop_Redux />
    </div>
  );
}

export default App;
