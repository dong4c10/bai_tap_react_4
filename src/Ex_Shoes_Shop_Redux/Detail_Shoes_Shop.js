import React, { Component } from "react";
import { connect } from "react-redux";

class Detail_Shoes_Shop extends Component {
  render() {
    let {
      id,
      name,
      alias,
      price,
      description,
      shortDescription,
      image,
      quantity,
    } = this.props.detailShoe;
    return (
      <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" class="px-6 py-3">
                Product name
              </th>
              <th scope="col" class="px-6 py-3">
                Price
              </th>
              <th scope="col" class="px-6 py-3">
                ShortDescription
              </th>
              <th scope="col" class="px-6 py-3">
                Quantity
              </th>
              <th scope="col" class="px-6 py-3">
                Image
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
              <th
                scope="row"
                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {name}
              </th>
              <td class="px-6 py-4">{price}</td>
              <td class="px-6 py-4">{shortDescription}</td>
              <td class="px-6 py-4">{quantity}</td>
              <td class="px-6 py-4">
                <img style={{ width: "25%" }} src={image} alt="" />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detailShoe: state.shoesReducer.detailShoe,
  };
};

export default connect(mapStateToProps)(Detail_Shoes_Shop);
