import React, { Component } from "react";
import { BUY_SHOE, DETAIL } from "./redux/constant/constant";
import { connect } from "react-redux";

export class Item_Shoes_Shop extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
        // <div className="container">
        //   <div className="col-4">
        //     <div classname="card text-left">
        //       <img
        //         classname="card-img-top"
        //         src={image}
        //         style={{ width: "25%" }}
        //       />
        //       <div classname="card-body">
        //         <h4 classname="card-title">{name}</h4>
        //         <p classname="card-text">{price}</p>
        //       </div>
        //       <button
        //         onClick={() => {
        //           this.props.handleWatchDetail(this.props.data);
        //         }}
        //         class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        //       >
        //         Xem Chi Tiết
        //       </button>
        //       <button
        //         onClick={() => {
        //           this.props.handleBuy(this.props.data);
        //         }}
        //         className="btn btn-secondary"
        //       >
        //         Mua
        //       </button>
        //     </div>
        //   </div>
      <div classname="container-fluid">
        <div class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
          <div>
            <img
              classname="text-center"
              src={image}
              style={{ width: "50%", marginLeft: "65px" }}
            />
          </div>
          <a href="#">
            <h5
              style={{ fontSize: "15px" }}
              class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white"
            >
              {name}
            </h5>
          </a>
          <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
            <span class="mb-2 text-l font-bold tracking-tight text-gray-900 dark:text-white">
              Price:
            </span>{" "}
            {price}
          </p>
          <button
            onClick={() => {
              this.props.handleWatchDetail(this.props.data);
            }}
            class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 m-2"
          >
            Xem Chi Tiết
            <svg
              aria-hidden="true"
              class="w-4 h-4 ml-2 -mr-1"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </button>
          <button
            onClick={() => {
                 this.props.handleBuy(this.props.data);
                 }}
            class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-success-700 rounded-lg hover:bg-success-800 focus:ring-4 focus:outline-none focus:ring-success-300 dark:bg-success-600 dark:hover:bg-success-700 dark:focus:ring-success-800 m-2"
          >
            Mua
            <path
              fill-rule="evenodd"
              d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
              clip-rule="evenodd"
            ></path>
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleWatchDetail: (data) => {
      // dispatch({
      //   type: DETAIL,
      //   payload: data,
      // });
      let action = {
        type: DETAIL,
        payload: data,
      };
      dispatch(action);
    },
    handleBuy: (data) => {
      let action = {
        type: BUY_SHOE,
        payload: data,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Shoes_Shop);
