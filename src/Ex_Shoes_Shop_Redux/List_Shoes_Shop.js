import React, { Component } from "react";
import Item_Shoes_Shop from "./Item_Shoes_Shop";
import { connect } from "react-redux";

class List_Shoes_Shop extends Component {
  renderListShoes = () => {
    return this.props.shoes.map((item, index) => {
      return <Item_Shoes_Shop key={index} data={item} />;
    });
  };
  render() {
    return <div className="grid grid-cols-2 gap-4">{this.renderListShoes()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoes: state.shoesReducer.shoesArr,
  };
};
export default connect(mapStateToProps, null)(List_Shoes_Shop);
