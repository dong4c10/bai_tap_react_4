import React, { Component } from "react";
import { connect } from "react-redux";

class CardShoes extends Component {
  render() {
    console.log("test: ", this.props.card);
    let { card, handleRemove, handleChangeAmount } = this.props;
    return (
      <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" class="px-6 py-3">
                Product name
              </th>
              <th scope="col" class="px-6 py-3">
                Amount
              </th>
              <th scope="col" class="px-6 py-3">
                Price
              </th>
              <th scope="col" class="px-6 py-3">
                Image
              </th>
              <th scope="col" class="px-6 py-3">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {card.map((item, index) => {
              return (
                // <tr key={index}>
                //   <td>{item.name}</td>

                //   <td>
                //     <button
                //       onClick={() => {
                //         handleChangeAmount(item, +1);
                //       }}
                //       class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
                //     >
                //       +
                //     </button>
                //     {item.soLuong}
                //     <button
                //       onClick={() => {
                //         handleChangeAmount(item, -1);
                //       }}
                //       class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded"
                //     >
                //       -
                //     </button>
                //   </td>

                //   <td>{item.price * item.soLuong}</td>

                //   <td>
                //     <img style={{ width: "50px" }} src={item.image} alt="" />
                //   </td>
                //   <td
                //     onClick={() => {
                //       handleRemove(item.id);
                //     }}
                //     class="bg-red-400 hover:bg-red-500 text-white font-bold py-2 px-4 rounded"
                //   >
                //     X{" "}
                //   </td>
                // </tr>

                <tr
                  key={index}
                  class="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                >
                  <th
                    scope="row"
                    class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {item.name}
                  </th>
                  <td class="px-6 py-4 text-center">
                    <button
                      onClick={() => {
                        handleChangeAmount(item, +1);
                      }}
                      class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded m-1 w-10"
                    >
                      +
                    </button>
                    {item.soLuong}
                    <button
                      onClick={() => {
                        handleChangeAmount(item, -1);
                      }}
                      class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded m-1 w-10"
                    >
                      -
                    </button>
                  </td>
                  <td class="px-6 py-4">{item.price * item.soLuong}</td>
                  <td class="px-6 py-4">
                    <img style={{ width: "50px" }} src={item.image} alt="" />
                  </td>
                  <td
                    onClick={() => {
                      handleRemove(item.id);
                    }}
                    class="px-6 py-4 text-red"
                  >
                    <a
                      href="#"
                      class="font-medium text-red-600 dark:text-red-500 hover:underline"
                    >
                      Xóa
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    card: state.shoesReducer.card,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (id) => {
      let action = {
        type: "DELETE_SHOE",
        payload: id,
      };
      dispatch(action);
    },
    handleChangeAmount: (shoe, option) => {
      let action = {
        type: "CHANGE_AMOUNT",
        payload: {
          shoe: shoe,
          option: option,
        },
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CardShoes);
