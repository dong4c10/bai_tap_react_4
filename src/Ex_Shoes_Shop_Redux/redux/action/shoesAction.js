//action creator
import { BUY_SHOE, DETAIL } from "../constant/constant";

export let viewDetailAction = (data) => {
  return {
    type: DETAIL,
    payload: data,
  };
};

export let buyShoesAction = (data) => {
  return {
    type: BUY_SHOE,
    payload: data,
  };
};
