import { shoesArr } from "../../data";
import {
  BUY_SHOE,
  CHANGE_AMOUNT,
  DELETE_SHOE,
  DETAIL,
} from "../constant/constant";

let initialState = {
  shoesArr: shoesArr,
  detailShoe: shoesArr[0],
  card: shoesArr,
};

export const shoesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case DETAIL: {
      state.detailShoe = payload;
      return {
        ...state,
      };
    }
    case DELETE_SHOE: {
      let cloneCard = state.card.filter((item) => {
        return item.id !== payload;
      });
      return { ...state, card: cloneCard };
    }
    case BUY_SHOE: {
      let cloneCard = [...state.card];
      let index = cloneCard.findIndex((item) => item.id === payload.id);
      if (index === -1) {
        let newShoe = { ...payload, soLuong: 1 };
        cloneCard.push(newShoe);
      } else {
        cloneCard[index].soLuong = cloneCard[index].soLuong + 1;
      }
      return { ...state, card: cloneCard };
    }
    case CHANGE_AMOUNT: {
      let { shoe, option } = payload;
      let cloneCard = [...state.card];
      let index = cloneCard.findIndex((item) => item.id === shoe.id);
      cloneCard[index].soLuong = cloneCard[index].soLuong + option;
      // soLuong có thể tăng hoặc giảm
      if (cloneCard[index].soLuong == 0) {
        //sau khi updatee soLuong, nếu bằng 0 thì xóa
        cloneCard.splice(index, 1);
      }
      return { ...state, card: cloneCard };
    }
    default:
      return state;
  }
};
